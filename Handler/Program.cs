﻿using RawRabbit.Configuration;
using RawRabbit.vNext;
using System;
using System.Threading.Tasks;

namespace Handler
{
    class Program
    {
        static void Main(string[] args)
        {
            MainAsync(args).Wait();
        }

        public static async Task MainAsync(string[] args)
        {
            var config = new RawRabbitConfiguration
            {
                Port = 32770, // port of docker image
                VirtualHost = "/",
            };

            var client = BusClientFactory.CreateDefault(config);

            client.SubscribeAsync<string>(async (msg, context) =>
            {
                Console.WriteLine(msg);
            });
        }
    }
}
