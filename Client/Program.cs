﻿using RawRabbit.Configuration;
using RawRabbit.vNext;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Client
{
    internal class Program
    {
        static void Main(string[] args)
        {
            MainAsync(args).Wait();
        }

        public static async Task MainAsync(string[] args)
        {
            var config = new RawRabbitConfiguration
            {
                Port = 32770, // port of docker image
                VirtualHost = "/",
            };

            var client = BusClientFactory.CreateDefault(config);

            var index = 0;
            while (true)
            {
                Thread.Sleep(1000);
                await client.PublishAsync($"Hello, world {index}!");
                index++;
            }
        }
    }
}
